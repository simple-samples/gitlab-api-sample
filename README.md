# Usage
## Running in Gitlab
* define Gitlab CD/CD variable in the project for `ACCESS_TOKEN` with a value of a Gitlab access token
## Runing locally
* create a .env file
* place an Gitlab access token in the .env for the variable `ACCESS_TOKEN`