import gitlab
import os
from envparse import env

if os.path.isfile('.env'):
    env.read_envfile()

ACCESS_TOKEN = env('ACCESS_TOKEN')

gl = gitlab.Gitlab('https://gitlab.com/', ACCESS_TOKEN)


def get_projects():
    projects = gl.projects.list(owned=True)
    for project in projects:
        print(project.name)


def main():
    get_projects()


main()

